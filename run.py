import uvicorn  # pylint: disable=E0401

# from devtools import debug


def main():
    print('Hola mundo! main')
    uvicorn.run('src.app.app:app', host='0.0.0.0',
                port=8050, reload=True, workers=4)


if __name__ == '__main__':
    main()
