# https://fastapi.tiangolo.com/tutorial/testing/

import json

from fastapi import status  # pylint: disable=E0401
from fastapi.testclient import TestClient  # pylint: disable=E0401

from src.app.app import app
from src.mockup.mockup_user import data_payload

client = TestClient(app)


def test_read_main():
    response = client.get('/')
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {'msg': 'Hello World'}


def test_create_user():
    response = client.post('/user/', data=json.dumps(data_payload))
    assert response.status_code == status.HTTP_201_CREATED
    assert response.json() == data_payload
