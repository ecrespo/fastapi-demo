# https://dev.to/ericchapman/python-fastapi-crash-course-533e
from debug_toolbar.middleware import \
    DebugToolbarMiddleware  # pylint: disable=E0401
from fastapi import FastAPI, status  # pylint: disable=E0401

from src.schemas.schema_user import User
from src.utils.loggerfactory import LoggerFactory

logger = LoggerFactory.get_logger('app.py', log_level='INFO')

app = FastAPI(title='My App', debug=True)
app.add_middleware(DebugToolbarMiddleware)


@app.post('/user/', response_model=User, status_code=status.HTTP_201_CREATED)
def create_user(user: User):  # dead: disable
    logger.info('Created user')
    return user


@app.get('/', status_code=status.HTTP_200_OK)
def read_main():  # dead: disable
    logger.info('Read Main')
    return {'msg': 'Hello World'}
