# mypy: ignore-errors
# https://medium.com/analytics-vidhya/camel-case-models-with-fast-api-and-pydantic-5a8acb6c0eee
from humps.main import camelize  # pylint: disable=E0401
from pydantic import BaseModel, Field  # pylint: disable=E0401,E0611


def to_camel(string):
    return camelize(string)


class CamelModel(BaseModel):  # pylint: disable=R0903
    class Config:  # pylint: disable=R0903 # dead: disable
        alias_generator = to_camel  # dead: disable
        allow_population_by_field_name = True  # dead: disable


class User(CamelModel):  # pylint: disable=R0903
    # first_name: str = Field(..., alias="firstName")
    # last_name: str = Field(None, alias="lastName")
    first_name: str
    last_name: str
    age: int = Field(None, ge=0, le=150)
