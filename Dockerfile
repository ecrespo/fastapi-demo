
FROM python:3.9-slim-buster as python-base

#FROM python:3.9



ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
ENV PIP_NO_CACHE_DIR=off
ENV PIP_DISABLE_PIP_VERSION_CHECK=on
ENV PIP_DEFAULT_TIMEOUT=100
#ENV POETRY_VERSION=1.1.7
#ENV POETRY_HOME="/opt/poetry"
#ENV POETRY_VIRTUALENVS_IN_PROJECT=true
#ENV POETRY_NO_INTERACTION=1
#ENV PYSETUP_PATH="/opt/pysetup"
#ENV VENV_PATH="/opt/pysetup/.venv"

#ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN mkdir /code
WORKDIR /code


COPY . /code/
COPY pyproject.toml  poetry.lock  /code/

ENV PYTHONPATH=${PYTHONPATH}:${PWD}

RUN pip install --no-cache-dir  --upgrade pip

RUN pip install --no-cache-dir  poetry

RUN poetry config virtualenvs.create true

RUN poetry install --no-dev --no-interaction --no-ansi

#RUN poetry install --no-root --no-interaction --no-ansi

FROM python-base as production

CMD ["poetry","run","python3","run.py"]

#CMD ["poetry","run","uvicorn", "src.app.app:app", "--host", "0.0.0.0","--reload", "--port", "8050"]
